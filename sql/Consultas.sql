-- IMPORTACIONES
CREATE TABLE `trescerritos_dev`.`importaciones` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `baja` INT NULL DEFAULT 0,
  `user_id` INT NULL,
  `tmstmp` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `user_upd` INT NULL,
  `tmstmp_upd` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
