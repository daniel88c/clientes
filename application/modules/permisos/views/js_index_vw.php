<script type="text/javascript">

	jQuery(document).ready(function($){

		$( "#sortable" ).sortable();
    	$( "#sortable" ).disableSelection();

    	$("#combo_tipo_variables").change(function(){
    		console.log("sdjsada");
    		$( "#combo_variables" ).html('<option>Espere...</option>');

			$.post( "<?php echo base_url()?>permisos/getVariables",{ tabla: $(this).val()},
			 function( data ) {
			 	$( "#combo_variables" ).html('');
			 	variables = jQuery.parseJSON(data);
			 	$( "#combo_variables" ).append( "<option value=''>Seleccione...</option>" );
			 	$.each(variables, function( index, variable ){
			 		$( "#combo_variables" ).append( "<option value='"+variable.id+"'>"+variable.nombre+"</option>" );
			 	});
			});

			$('#tabla').val($(this).val());

    	});

    	$("#combo_variables").change(function(){
    		$("#nuevo_valor").val($("#combo_variables option:selected").text());
    		$("#id_actual").val($(this).val());
    		$("#form-editar").show();
    	});

    	$("#borrar").click(function(){
    		if ($(this).prop("checked"))
    			$("#mensaje").show();
    		else
    			$("#mensaje").hide();
    	});
   	});

	function eliminar(norma_id){
		Swal.fire({
		  title: 'Seguro que desea eliminar',
		  text: "Esta acción no se puede revertir",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si, eliminar!',
		  cancelButtonText: 'Cancelar'
		}).then((result) => {
		  if (result.value) {
		    Swal.fire(
		      
		    	jQuery.post("<?php echo site_url('normas/baja') ?>",  
		            {norma_id : norma_id},
		            function(data){
		                Swal.fire(
					      'Eliminado!',
					      'La norma ha sido eliminada.',
					      'success'
					    )
					    setTimeout(function () {
					        location.reload(true);
					      }, 500);
		            }))
		  }
		})
	}


</script>