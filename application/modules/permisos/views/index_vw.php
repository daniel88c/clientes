<div class="container">
	<div class="row">
		<div id="infoMessage">
			<?php if($message = $this->session->flashdata('message'))
		      echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
		        <strong>'.$message.'</strong>
		        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>';
		    ?>
		</div>
	</div>
	<div class="card-columns" id="my-masonry">
		<div class="">
			<div class="x_panel">
				<div class="x_title">
		            <h2><i class="fa fa-group"></i> Grupos de Usuario</h2>
		            <ul class="nav navbar-right panel_toolbox">
		              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		              </li>
		            </ul>
		            <div class="clearfix"></div>
		        </div>

				<div class="x_content">
					<table class="table table-striped jambo_table bulk_action">
						<thead>
							<tr>
								<th>#ID</th>
								<th>Nombre</th>
								<th style="width: 100px">Acciones</th>
							</tr>
						</thead>
						
						<?php foreach($grupos as $g){
							echo "<tr>";
							echo "<td>".$g->id."</td><td>".$g->name."</td>";
							echo "<td><a class='badge badge-primary' href='auth/edit_group/".$g->id."'>Editar</a><br>";
							echo "<a class='badge badge-danger' href='".base_url()."welcome/set_baja/groups/".$g->id."'>Eliminar</a></td>";
							echo "</tr>";
						}?>
					</table>
					<a href="<?php echo base_url()?>auth/create_group"><button class="btn btn-sm btn-info">Nuevo Grupo</button></a>
				</div>
			</div>
		</div>

		<div class="">
			<div class="x_panel">
				<div class="x_title">
		            <h2><i class="fa fa-cogs"></i> Variables del sistema</h2>
		            <ul class="nav navbar-right panel_toolbox">
		              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		              </li>
		            </ul>
		            <div class="clearfix"></div>
		        </div>
		        <div class="x_content">
					<select class="form-control select-live" id="combo_tipo_variables">
						<option>Seleccione...</option>
						<option value="inv_area">Areas</option>
						<option value="inv_funcion">Función</option>
						<option value="inv_tipo">Tipo</option>
						<option value="inv_marca">Marcas</option>
						<option value="inv_modelo">Modelos</option>
					</select><br><br>

					<select class="form-control select-live" id="combo_variables">
						
					</select><br><br>
					<form style="display: none" id="form-editar" action="<?php echo base_url()?>permisos/update_variable" method="post">
						<input type="text" name="nuevo_valor" id="nuevo_valor" value="" class="form-control" required>
						<input type="hidden" name="id_actual" id="id_actual" value="">
						<input type="hidden" name="tabla" id="tabla" value=""><br>
						<h5><input type="checkbox" name="borrar" id="borrar"> Borrar<br></h5>
						<span id="mensaje" class="text-danger" style="display: none;"><i>* Advertencia de borrado</i></span><br>
						<button type="submit" class="btn btn-sm btn-info">Guardar</button>

					</form>
				</div>
			</div>
		</div>

		<div class="">
			<div class="x_panel">
				<div class="x_title">
		            <h2><i class="fa fa-reorder"></i> Menu: Orden</h2>
		            <ul class="nav navbar-right panel_toolbox">
		              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		              </li>
		            </ul>
		            <div class="clearfix"></div>
		        </div>
		        <div class="x_content">
					<form action="<?php echo base_url()?>permisos/reordenar" method="POST">
						<table id="tb-reording" class="table">
							<tr><td><ul class="reording-menu">
									<?php foreach($menus as $m)
										echo "<li class='ui-state-default ui-sortable-handle'>".$m->orden."</li>";
									?>
								</ul></td><td><ul id="sortable" class="reording-menu">
									<?php $i=0; foreach($menus as $m){
										
										echo '<li class="ui-state-default"><i class="'.$m->icono.'"></i> '.$m->nombre.'<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
										<input type="hidden" name="items[]" value="'.$m->id.'">';
										if($m->nombre !="Home"){
											if ($m->dashboard) {
												echo '<label class="lb-in-home">
												<input name="dash-'.$m->id.'" type="checkbox" class="js-switch" checked /> En Home
											</label>';
											}
											else{
												echo '<label class="lb-in-home">
												<input name="dash-'.$m->id.'" type="checkbox" class="js-switch" /> En Home
												</label>';
											}
										}
																	
										echo '</li>';
									$i++; }?>
								</ul></td></tr>
						</table>
						<button type="submit" class="btn btn-sm btn-info">Guardar</button>
					</form>
				</div>
			</div>			
		</div>
	</div>
</div>