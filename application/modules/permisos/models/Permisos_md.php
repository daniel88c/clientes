<?php
class Permisos_md extends CI_Model{
	
	function __construct(){

		parent::__construct();

	}

	function getAll(){
		$query = $this->db->select("*")
		->where("baja", 0)
		->get("norma");
		return $query->result();
	}
	
	function reordenar($item, $j, $dash){
		$data = array(
			'orden' => $j,
			'dashboard' => $dash,
			'user_upd' => $this->session->userdata('user_id'),
			'tmstmp_upd' => date('Y-m-d H:i:s'),
		);
		
		$this->db->where('id',$item)
		->update('menus', $data);
		return $data;
	}

	public function setPermisos($grupo_id, $menu_id){
		$data = array(
				'grupo_id' => $grupo_id,
				'menu_id' => $menu_id
				);

		$this->db->insert('permisos', $data);
		$permiso_id = $this->db->insert_id();

		return $permiso_id;
	}

	public function removePermisos($grupo_id, $menu_id){
		$this->db->where('grupo_id', $grupo_id);
		$this->db->delete('permisos');
	}

	public function getPermisosByGroupId($grupos){
		$query = $this->db->select("per.menu_id, menu.*")
		->join("menus menu","menu.id = per.menu_id", "inner")
		->where_in('grupo_id', $grupos)
		->group_by('menu_id')
		->order_by('orden', 'asc')
		->get("permisos per");
		return $query->result();
	}

	public function getSubmenus($menu_id){
		$query = $this->db->select("menu.*")
		->where('parent_id', $menu_id)
		->order_by('orden', 'asc')
		->get("menus menu");
		return $query->result();
	}

	public function getAllByTabla($tabla){
		$query = $this->db->select("*")
		->where('baja', 0)
		->get($tabla);
		return $query->result();
	}

	public function update_variable($tabla, $id_actual, $nuevo_valor, $baja){
		$data = array(
			'nombre' => $nuevo_valor,
			'baja' => $baja,
			'user_upd' => $this->session->userdata('user_id'),
			'tmstmp_upd' => date('Y-m-d H:i:s'),
		);
		$this->db->where('id',$id_actual)
		->update($tabla, $data);
		return $data;
	}
}

?>