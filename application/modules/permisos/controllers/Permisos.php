<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permisos extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('general_model', 'general_md');
		$this->load->model('permisos/permisos_md');
	}

	public function index()
	{
		$username = $this->session->userdata('username');

		$datos_content["menus"] = $this->general_md->getAll('menus','*',$where='baja = 0 and parent_id = 0',$order= 'orden' ,$array = null);
		$grupos = $this->general_md->getAll('groups','id, name',$where='baja = 0',$order= null ,$array = null);


		$datos_content["username"] = $username;
		$datos_content["grupos"] = $grupos;
		$datos_content["title"] = 'Configuración';
		$datos_content["jscript"] = $this->load->view('permisos/js_index_vw', null, true);

		$datos_plantilla["contenido_main"] = $this->load->view('permisos/index_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}

	public function reordenar(){
		$items = $this->input->post('items');
		$j=1;
		foreach ($items as $item) {
			$dash = $this->input->post('dash-'.$item);
			if ($dash) 
				$dash = 1;
			else
				$dash = 0;

			$this->permisos_md->reordenar($item, $j, $dash);
			$j++;
		}

		$this->session->set_flashdata('message', 'Orden cambiado con exito!');
		redirect('permisos/index', 'refresh');
	}

	public function create(){
		$username = $this->session->userdata('username');

		$datos_content["username"] = $username;
		$datos_content["title"] = 'Nueva Norma';
		$datos_content["jscript"] = $this->load->view('normas/js_create_vw', null, true);

		$datos_plantilla["contenido_main"] = $this->load->view('normas/create_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}

	public function guardar(){
		$nombre = $this->input->post('nombre');

		$this->normas_md->alta_norma($nombre);

		$this->session->set_flashdata('exito', 'Norma creada con exito!');
		redirect('normas/index', 'refresh');
		
	}

	public function edit($norma_id){
		$username = $this->session->userdata('username');

		$norma = $this->normas_md->getNormaById($norma_id);

		$datos_content["username"] = $username;
		$datos_content["norma"] = $norma;
		$datos_content["jscript"] = $this->load->view('normas/js_edit_vw', null, true);

		$datos_plantilla["contenido_main"] = $this->load->view('normas/edit_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}

	public function actualizar(){
		$norma_id = $this->input->post('norma_id');
		$nombre = $this->input->post('nombre');

		$this->normas_md->update_norma($norma_id, $nombre);

		$this->session->set_flashdata('exito', 'Norma actualizada con exito!');
		redirect('normas/index', 'refresh');
	}

	public function baja(){
		$norma_id = $this->input->post('norma_id');
		$this->general_md->baja('norma', $norma_id);
		echo "1";

	}

	public function getVariables(){
		$tabla = $this->input->post('tabla');	
		$resultados = $this->permisos_md->getAllByTabla($tabla);

		echo json_encode($resultados);
	}

	public function update_variable(){

		$nuevo_valor = $this->input->post('nuevo_valor');
		$id_actual = $this->input->post('id_actual');
		$tabla = $this->input->post('tabla');
		$borrar = $this->input->post('borrar');

		if ($borrar)
			$baja = 1;
		else
			$baja = 0;

		$this->permisos_md->update_variable($tabla, $id_actual, $nuevo_valor, $baja);
		$this->session->set_flashdata('message', 'Variables actualziada con éxito!');
		redirect('permisos/index', 'refresh');
	
	}
}
