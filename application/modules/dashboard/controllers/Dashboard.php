<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('general_model', 'general_md');	
		$this->load->model('auth/ion_auth_model','ion_auth');	
		$this->load->model('permisos/permisos_md');
	}

	public function index()
	{

		$username = $this->session->userdata('username');

		$datos_content["username"] = $username;
		//$grupos = $this->auth_md->get_users_groups($this->session->userdata('id'));
		$grupos = $this->ion_auth->get_users_groups()->result();
		$i = 0;
		foreach ($grupos as $gr) {
			$groups[$i] = $gr->id;
			$i++;
		}

		$datos_content["permisos"] = $this->permisos_md->getPermisosByGroupId($groups);
		$datos_content["jscript"] = $this->load->view('dashboard/js_index_vw', null, true);
		$datos_plantilla["contenido_main"] = $this->load->view('dashboard/index_2_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}
}
