<?php
class Dashboard_md extends CI_Model{
	
	function __construct(){

		parent::__construct();

	}

	function getAll(){
		$query = $this->db->select("*")
		->where("baja", 0)
		->get("norma");
		return $query->result();
	}
	
	function reordenar($item, $j, $dash){
		$data = array(
			'orden' => $j,
			'dashboard' => $dash,
			'user_upd' => $this->session->userdata('user_id'),
			'tmstmp_upd' => date('Y-m-d H:i:s'),
		);
		
		$this->db->where('id',$item)
		->update('menus', $data);
		return $data;
	}

	function setPermisos($grupo_id, $menu_id){
		$data = array(
				'grupo_id' => $grupo_id,
				'menu_id' => $menu_id
				);

		$this->db->insert('permisos', $data);
		$permiso_id = $this->db->insert_id();

		return $permiso_id;
	}

	function removePermisos($grupo_id, $menu_id){
		$this->db->where('grupo_id', $grupo_id);
		$this->db->delete('permisos');
	}

	function getNormaById($norma_id){
		$query = $this->db->select("*")
		->where("id", $norma_id)
		->get("norma");
		return $query->row();
	}

	function update_norma($norma_id, $nombre){
		$data = array(
					'nombre' => $nombre,
					'user_upd' => $this->session->userdata('user_id'),
					'tmstmp_upd' => date('Y-m-d H:i:s'),
					);
			
			$this->db->where('id',$norma_id)
			->update('norma', $data);
			return $data;
	}


	
	function update_domicilio($domicilio_id
							 ,$provincia_id
							 ,$departamento_id
							 ,$comuna_id
							 ,$localidad_id
							 ,$otra_localidad
							 ,$municipio_id
							 ,$calle
							 ,$numero
							 ,$piso
							 ,$depto
							 ,$cpostal
							 ,$como_llegar
							 ){
	
			$data = array(
					'provincia_id' => $provincia_id,
					'departamento_id' => $departamento_id,
					'comuna_id' => $comuna_id,
					'localidad_id' => $localidad_id,
					'otra_localidad' => $otra_localidad,									
					'calle' => trim($calle),
					'numero' => trim($numero),
					'piso' => trim($piso),
					'depto' => trim($depto),
					'codigo_postal' => trim($cpostal),				
					'como_llegar' => trim($como_llegar),
					'user_upd' => $this->session->userdata('user_id')
					);
			
			$this->db->where('id',$domicilio_id)
			->update('sipp_domicilio', $data);
			return $data;
	}
	

	function getDeptoByProvincia($provincia_id)
	{
		$query = $this->db->select("departamento.id, departamento.nombre")
		->join("departamento","departamento.cod_prov = provincia.cod_prov", "inner")
		->where("provincia.id", $provincia_id)
		->get("provincia");
		$data = $query->result();
		return $data;
	}

	function getLocalidadByDepto($depto_id)
	{
		$query = $this->db->select("localidad.id, localidad.nombre")
		->join("localidad","localidad.cod_depto = departamento.cod_depto", "inner")
		->join("provincia", "departamento.cod_prov = provincia.cod_prov AND provincia.cod_prov = localidad.cod_prov","inner")
		->where("departamento.id", $depto_id)
		->get("departamento");
		$data = $query->result();
		return $data;
	}

	function getComunaByDepto($depto_id)
	{
		$query = $this->db->select("comuna.id, comuna.nombre")
		->join("comuna_municipio comuna","comuna.cod_depto = departamento.cod_depto", "inner")
		->join("provincia", "departamento.cod_prov = provincia.cod_prov AND provincia.cod_prov = comuna.cod_prov","inner")
		->where("departamento.id", $depto_id)
		->group_by("comuna.id")
		->get("departamento");
		$data = $query->result();
		return $data;
	}


	function getComunaByDeptoAndLocalidad($depto_id,$localidad_id)
	{
		$query = $this->db->select("comuna.id, comuna.nombre")		
		->join("comuna_municipio comuna","comuna.cod_depto = departamento.cod_depto", "inner")
		->join("localidad","localidad.cod_mun = comuna.cod_mun", "inner")
		->join("provincia", "departamento.cod_prov = provincia.cod_prov AND provincia.cod_prov = comuna.cod_prov","inner")
		->where("localidad.id", $localidad_id)
		->where("departamento.id", $depto_id)
		->limit(1)
		->get("departamento");
		$data = $query->result();
		return $data;
	}

	function getLocalidadByDeptoAndComuna($depto_id,$comuna_id)
	{
		$query = $this->db->select("localidad.id, localidad.nombre")		
		->join("comuna_municipio comuna","comuna.cod_depto = departamento.cod_depto", "inner")
		->join("localidad","localidad.cod_mun = comuna.cod_mun", "inner")
		->join("provincia", "departamento.cod_prov = provincia.cod_prov AND provincia.cod_prov = comuna.cod_prov","inner")
		->where("comuna.id", $comuna_id)
		->where("departamento.id", $depto_id)
		->get("departamento");
		$data = $query->result();
		return $data;
	}


}

?>