<div class="container">
	<br>
	<h1>Bienvenido</h1>
	<hr>
</div>
<div class="page_content_wrap">
    <div class="content_wrap">
        <div class="content">
            <?php foreach ($permisos as $menu) :
                if ($menu->dashboard):
                ?>
                    <div class="col-md-3">
                        <div class="x_panel" style="background: #ffffff; border-bottom:6px solid #<?php echo $menu->color?>;border-radius: 7px;">
                            <div class="x_content">
                                <i class="icons <?php echo $menu->icono; ?>" style="color: #E5E5D5;right: 0;position: absolute;font-size: 80px;top: 37px;"></i>
                                <h4 class="brief"><b><?php echo $menu->nombre?></b></h4>
                                <hr>
                                <div class="left col-md-12 col-sm-12">
                                    <div class="right col-md-12 col-sm-12">
                                    <?php if($menu->link !='#'):?>
                                        
                                            <a href="<?php echo base_url().$menu->link?>"><h6><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Listar</h6></a>
                                            <a href="<?php echo base_url().$menu->link?>/create<?php if($menu->link == 'auth') echo '_user'?>"><h6><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo</h6></a>
                                        

                                    <?php else:
                                            $submenus = $this->permisos_md->getSubmenus($menu->id);
                                            foreach ($submenus as $sub ){
                                                echo '<a href="'.base_url().$sub->link.'"><h6><span class="glyphicon glyphicon-list" aria-hidden="true"></span> '.$sub->nombre.'</h6></a>';
                                            }
                                        

                                        endif;?>
                                    </div>
                                    
                                    <!-- <div class="right col-md-4 col-sm-4">
                                        <div class="fa-hover">
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                endif;
            endforeach;
            ?>
        </div>
    </div>
</div>	