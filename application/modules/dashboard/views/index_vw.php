<div class="container">
	<br>
	<h1>Bienvenido</h1>
	<hr>
</div>
<div class="page_content_wrap">
    <div class="content_wrap">
        <div class="content">
           
        <article class="post_item post_item_single post_featured_left post_format_standard page hentry">
            <section class="post_content">
                <div class="vc_row wpb_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="sc_team sc_team_style_2">
                                    <div class="sc_columns columns_wrap">
                                        <div class="column-1_4">
                                        <div class="x_panel">
                                        <div class="x_content">
                                        <div class="col-md-12 col-sm-12"></div>                                            
                                        <div class="clearfix"></div>
                                        <!-- User-->
                                        <div class="col-md-4 col-sm-4  profile_details">
                                            <div class="well profile_view usuario-card">
                                            <div class="col-sm-12">
                                                <h4 class="brief"><i>USUARIOS</i></h4>
                                                <div class="left col-md-7 col-sm-7">
                                                <h2>Lorem Impsu</h2>
                                                <p><strong>About: </strong> Web Designer / UX / Graphic Artist / Coffee Lover </p>
                                                <ul class="list-unstyled">
                                                    <li><i class="fa fa-building"></i> Address: </li>
                                                    <li><i class="fa fa-phone"></i> Phone #: </li>
                                                </ul>
                                                </div>
                                                <div class="right col-md-5 col-sm-5 text-center">
                                                    <div class="fa-hover"><i class="icons fa fa-user"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=" profile-bottom text-center">
                                                <div class="col-sm-6 emphasis">
                                                    <div class="bs-glyphicons">
                                                    <ul class="bs-glyphicons-list">
                                                    <a href="<?php echo base_url()?>auth">
                                                    <li>
                                                        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                                        <span class="glyphicon-class">Listar Usuarios </span>
                                                    </li>
                                                    </a>
                                                    </ul>
                                                    </div>
                                                </div>
                                                <div class=" col-sm-6 emphasis">
                                                    <div class="bs-glyphicons">
                                                        <ul class="bs-glyphicons-list">
                                                        <a href="<?php echo base_url()?>auth/create_user">
                                                        <li>
                                                            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                                            <span class="glyphicon-class">Crear Usuario</span>
                                                        </li>
                                                        </a>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <!--Client-->
                                        <div class="col-md-4 col-sm-4  profile_details">
                                            <div class="well profile_view cliente-card">
                                            <div class="col-sm-12">
                                                <h4 class="brief"><i>CLIENTES</i></h4>
                                                <div class="left col-md-7 col-sm-7">
                                                <h2>Lorem Impsu</h2>
                                                <p><strong>About: </strong> Web Designer / UX / Graphic Artist / Coffee Lover </p>
                                                <ul class="list-unstyled">
                                                    <li><i class="fa fa-building"></i> Address: </li>
                                                    <li><i class="fa fa-phone"></i> Phone #: </li>
                                                </ul>
                                                </div>
                                                <div class="right col-md-5 col-sm-5 text-center">
                                                    <div class="fa-hover"><i class="icons fa fa-users"></i>
                                                    </div>                                                
                                                </div>
                                            </div>
                                            <div class=" profile-bottom text-center">
                                                <div class=" col-sm-6 emphasis">
                                                    
                                                    <div class="bs-glyphicons">
                                                        <ul class="bs-glyphicons-list">
                                                        <a href="<?php echo base_url()?>clientes">
                                                        <li>
                                                            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                                            <span class="glyphicon-class">Listar Clientes</span>
                                                        </li>
                                                        </a>
                                                        </ul>
                                                    </div>

                                                </div>
                                                <div class=" col-sm-6 emphasis">

                                                    <div class="bs-glyphicons">
                                                        <ul class="bs-glyphicons-list">
                                                        <a href="<?php echo base_url()?>clientes/create">
                                                        <li>
                                                            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                                            <span class="glyphicon-class">Crear Cliente</span>
                                                        </li>
                                                        </a>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- Responsable-->
                                        <div class="col-md-4 col-sm-4  profile_details">
                                            <div class="well profile_view responsable-card">
                                            <div class="col-sm-12">
                                                <h4 class="brief"><i>RESPONSABLES</i></h4>
                                                <div class="left col-md-7 col-sm-7">
                                                <h2>Lorem Impsu</h2>
                                                <p><strong>About: </strong> Web Designer / UX / Graphic Artist / Coffee Lover </p>
                                                <ul class="list-unstyled">
                                                    <li><i class="fa fa-building"></i> Address: </li>
                                                    <li><i class="fa fa-phone"></i> Phone #: </li>
                                                </ul>
                                                </div>
                                                <div class="right col-md-5 col-sm-5 text-center">
                                                    <div class="fa-hover"><i class="icons fa fa-briefcase"></i>
                                                    </div>                                                
                                                </div>
                                            </div>
                                            <div class=" profile-bottom text-center">
                                                <div class=" col-sm-6 emphasis">

                                                    <div class="bs-glyphicons">
                                                        <ul class="bs-glyphicons-list">
                                                        <a href="<?php echo base_url()?>responsables">
                                                        <li>
                                                            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                                            <span class="glyphicon-class">Listar Responsables</span>
                                                        </li>
                                                        </a>
                                                        </ul>
                                                    </div>

                                                </div>
                                                <div class=" col-sm-6 emphasis">
                                                    <div class="bs-glyphicons">
                                                        <ul class="bs-glyphicons-list">
                                                        <a href="<?php echo base_url()?>responsables/create">
                                                        <li>
                                                            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                                            <span class="glyphicon-class">Crear Responsable</span>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <!---Normas-->
                                        <div class="col-md-4 col-sm-4  profile_details">
                                            <div class="well profile_view norma-card">
                                            <div class="col-sm-12">
                                                <h4 class="brief"><i>NORMAS</i></h4>
                                                <div class="left col-md-7 col-sm-7">
                                                <h2>Lorem Impsu</h2>
                                                <p><strong>About: </strong> Web Designer / UX / Graphic Artist / Coffee Lover </p>
                                                <ul class="list-unstyled">
                                                    <li><i class="fa fa-building"></i> Address: </li>
                                                    <li><i class="fa fa-phone"></i> Phone #: </li>
                                                </ul>
                                                </div>
                                                <div class="right col-md-5 col-sm-5 text-center">
                                                    
                                                    <div class="fa-hover"><i class="icons fa fa-book"></i>
                                                    </div>                                                
                                                </div>
                                            </div>
                                            <div class=" profile-bottom text-center">
                                                <div class=" col-sm-6 emphasis">
                                                    
                                                    <div class="bs-glyphicons">
                                                        <ul class="bs-glyphicons-list">
                                                        <a href="<?php echo base_url()?>normas">    
                                                        <li>
                                                            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                                            <span class="glyphicon-class">Listar Normas</span>
                                                        </li>
                                                        </a>
                                                        </ul>
                                                    </div>

                                                </div>
                                                <div class=" col-sm-6 emphasis">
                                                    <div class="bs-glyphicons">
                                                        <ul class="bs-glyphicons-list">
                                                        <a href="<?php echo base_url()?>normas/create">
                                                        <li>
                                                            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                                            <span class="glyphicon-class">Crear Norma</span>
                                                        </li>
                                                        </a>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>                                            
                                        <!--///-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>

                <section class="related_wrap related_wrap_empty"></section>
        </div>
    </div>
</div>	