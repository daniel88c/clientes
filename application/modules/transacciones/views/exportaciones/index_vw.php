<div class="x_panel">
<div class="container">
	<a href="<?php echo base_url()?>transacciones/exportaciones/compra"><button class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Nueva Compra </button></a>
	<a href="<?php echo base_url()?>transacciones/exportaciones/venta"><button class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Nueva Venta </button></a>
	<table class="datatable table table-striped jambo_table bulk_action">
		<thead>
			<tr>
				<th>#ID</th>
				<th>Nombre</th>
				<th>Acciones</th>
			</tr>
		</thead>
		
		<?php foreach($exportaciones as $n){
			echo "<tr>";
			echo "<td>".$n->id."</td>";
			echo "<td>-</td>";
			echo "<td><br>";
			echo "<a class='badge badge-danger' onclick='eliminar(".$n->id.")'>Eliminar</a></td>";
			echo "</tr>";
		}?>
	</table>
</div>
</div>