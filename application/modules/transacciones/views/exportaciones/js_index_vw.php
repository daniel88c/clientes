<script type="text/javascript">

	jQuery(document).ready(function($){
		console.log("script cargado!");

		
	});

	function eliminar(norma_id){
		Swal.fire({
		  title: 'Seguro que desea eliminar',
		  text: "Esta acción no se puede revertir",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si, eliminar!',
		  cancelButtonText: 'Cancelar'
		}).then((result) => {
		  if (result.value) {
		    Swal.fire(
		      
		    	jQuery.post("<?php echo site_url('normas/baja') ?>",  
		            {norma_id : norma_id},
		            function(data){
		                Swal.fire(
					      'Eliminado!',
					      'La norma ha sido eliminada.',
					      'success'
					    )
					    setTimeout(function () {
					        location.reload(true);
					      }, 500);
		            }))
		  }
		})
	}
</script>