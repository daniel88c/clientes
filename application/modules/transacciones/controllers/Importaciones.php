<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Importaciones extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('general_model', 'general_md');		
		$this->load->model('transacciones/importaciones_md');
	}

	public function index()
	{
		$username = $this->session->userdata('username');

		$importaciones = $this->importaciones_md->getAll();

		$datos_content["username"] = $username;
		$datos_content["importaciones"] = $importaciones;
		$datos_content["title"] = 'importaciones';
		$datos_content["jscript"] = $this->load->view('transacciones/importaciones/js_index_vw', null, true);

		$datos_plantilla["contenido_main"] = $this->load->view('transacciones/importaciones/index_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}

	public function compra(){
		$username = $this->session->userdata('username');

		$datos_content["username"] = $username;
		$datos_content["title"] = 'Importación - Compra';
		$datos_content["jscript"] = $this->load->view('transacciones/importaciones/js_compra_vw', null, true);

		$datos_plantilla["contenido_main"] = $this->load->view('transacciones/importaciones/compra_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}

	public function venta(){
		$username = $this->session->userdata('username');

		$datos_content["username"] = $username;
		$datos_content["title"] = 'Importación Venta';
		$datos_content["jscript"] = $this->load->view('transacciones/importaciones/js_venta_vw', null, true);

		$datos_plantilla["contenido_main"] = $this->load->view('transacciones/importaciones/venta_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}

	public function guardar(){
		$nombre = $this->input->post('nombre');

		$this->importaciones_md->alta_norma($nombre);

		$this->session->set_flashdata('exito', 'Norma creada con exito!');
		redirect('normas/index', 'refresh');
		
	}

	public function edit($norma_id){
		$username = $this->session->userdata('username');

		$norma = $this->importaciones_md->getNormaById($norma_id);

		$datos_content["username"] = $username;
		$datos_content["norma"] = $norma;
		$datos_content["jscript"] = $this->load->view('normas/js_edit_vw', null, true);

		$datos_plantilla["contenido_main"] = $this->load->view('normas/edit_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}

	public function actualizar(){
		$norma_id = $this->input->post('norma_id');
		$nombre = $this->input->post('nombre');

		$this->importaciones_md->update_norma($norma_id, $nombre);

		$this->session->set_flashdata('exito', 'Norma actualizada con exito!');
		redirect('normas/index', 'refresh');
	}

	public function baja(){
		$norma_id = $this->input->post('norma_id');
		$this->general_md->baja('norma', $norma_id);
		echo "1";

	}
}
