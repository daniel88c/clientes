<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exportaciones extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('general_model', 'general_md');		
		$this->load->model('transacciones/exportaciones_md');
	}

	public function index()
	{
		$username = $this->session->userdata('username');

		$exportaciones = $this->exportaciones_md->getAll();

		$datos_content["username"] = $username;
		$datos_content["exportaciones"] = $exportaciones;
		$datos_content["title"] = 'exportaciones';
		$datos_content["jscript"] = $this->load->view('transacciones/exportaciones/js_index_vw', null, true);

		$datos_plantilla["contenido_main"] = $this->load->view('transacciones/exportaciones/index_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}

	public function compra(){
		$username = $this->session->userdata('username');

		$datos_content["username"] = $username;
		$datos_content["title"] = 'Exportación - Compra';
		$datos_content["jscript"] = $this->load->view('transacciones/exportaciones/js_compra_vw', null, true);

		$datos_plantilla["contenido_main"] = $this->load->view('transacciones/exportaciones/compra_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}

	public function venta(){
		$username = $this->session->userdata('username');

		$datos_content["username"] = $username;
		$datos_content["title"] = 'Exportación Venta';
		$datos_content["jscript"] = $this->load->view('transacciones/exportaciones/js_venta_vw', null, true);

		$datos_plantilla["contenido_main"] = $this->load->view('transacciones/exportaciones/venta_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}

	public function guardar(){
		$nombre = $this->input->post('nombre');

		$this->exportaciones_md->alta_norma($nombre);

		$this->session->set_flashdata('exito', 'Norma creada con exito!');
		redirect('normas/index', 'refresh');
		
	}

	public function edit($norma_id){
		$username = $this->session->userdata('username');

		$norma = $this->exportaciones_md->getNormaById($norma_id);

		$datos_content["username"] = $username;
		$datos_content["norma"] = $norma;
		$datos_content["jscript"] = $this->load->view('normas/js_edit_vw', null, true);

		$datos_plantilla["contenido_main"] = $this->load->view('normas/edit_vw', $datos_content, true);
		
		$this->load->view('template', $datos_plantilla);
	}

	public function actualizar(){
		$norma_id = $this->input->post('norma_id');
		$nombre = $this->input->post('nombre');

		$this->exportaciones_md->update_norma($norma_id, $nombre);

		$this->session->set_flashdata('exito', 'Norma actualizada con exito!');
		redirect('normas/index', 'refresh');
	}

	public function baja(){
		$norma_id = $this->input->post('norma_id');
		$this->general_md->baja('norma', $norma_id);
		echo "1";

	}
}
