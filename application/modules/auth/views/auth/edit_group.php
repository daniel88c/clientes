<div class="container">
<p><?php echo lang('edit_group_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open(current_url());?>
 <div class="col-md-6">
      <div class="x_panel">
            <div class="x_content">
                  <div class="form-group">
                        <p>
                              <?php echo lang('edit_group_name_label', 'group_name');?> <br />
                              <?php echo form_input($group_name);?>
                        </p>

                        <p>
                              <?php echo lang('edit_group_desc_label', 'description');?> <br />
                              <?php echo form_input($group_description);?>
                        </p>

                        <p>
                        
                  </div>
            </div>
      </div>
</div>

<div class="col-sm-6">
            <div class="x_panel">
                  <div class="x_content">
                        <label>Permisos</label><br>
                        
                        <?php foreach ($menus as $m): ?>
                        <div class="checkbox" style="float: left;margin-right: 15px;border-right: 1px solid;padding-right: 15px;height:22px;">
                              <label class="">
                                    <div class="icheckbox_flat-green" style="position: relative;">
                                          <?php $check = 0;
                                           foreach($user_menu as $us)
                                                if ($us->menu_id == $m->id) {
                                                      $check = 1;
                                                }
                                          ?>
                                    <input name="menus[]" type="checkbox" class="flat" <?php if ($check) echo "checked"?> value="<?php echo $m->id ?>" style="position: absolute; opacity: 0;float: left;">
                                    <ins class="iCheck-helper icheckbox_flat-green checked" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                    </ins>
                                    </div> <?php echo $m->nombre ?>
                              </label>
                        </div>
                  <?php endforeach; ?>
                  </div>
            </div>
      </div>
</div>
 <p>
      <?php echo form_submit('submit', lang('edit_group_submit_btn'), 'class="btn btn-sm btn-success"');?>
      <a type="button" class="btn btn-sm btn-danger" href="javascript:history.back(-1);" title="Cancelar">Cancelar</a>

      </p>

<?php echo form_close();?>
</div>