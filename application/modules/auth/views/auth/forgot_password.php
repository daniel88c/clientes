<!DOCTYPE html>
<html lang="en">
<head>

  <title>Login INDICIO</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="<?php echo base_url()?>assets/template/css/login2/images/favicon.png" />
  <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/login2/css/main.css' type='text/css' media='all' />
  <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/login2/css/util.css' type='text/css' media='all' />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/template/css/login2/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/template/css/login2/css/util.css">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>

<body>

<div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100 p-t-85 p-b-20">
            <span class="login100-form-avatar">
                  <img src="<?php echo base_url()?>assets/template/css/login2/images/logo.png" alt="AVATAR">
            </span>
            <h1><?php echo lang('forgot_password_heading');?></h1>
            <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

            <div id="infoMessage"><?php echo $message;?></div>

            <?php echo form_open("auth/forgot_password");?>
                  <div class="wrap-input100 validate-input m-t-85 m-b-35 olvidar" data-validate = "Enter username">
                      <div>
                        <p>
                        <label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
                        <?php echo form_input($identity);?>
                        </p>
                      </div>
                  </div>      
                  <div class="boton-login">
                        <p style="display:flex"><?php echo form_submit('submit', lang('forgot_password_submit_btn'),'class="btn-forgot-n login100-form-btn"');?>
                        <a type="button" class="btn-forgot-n login100-form-btn" href="javascript:history.back(-1);" title="Cancelar">Cancelar</a>
                        </p>
                  </div>
            <?php echo form_close();?>
            <div id="dropDownSelect1"></div>    
      </div>
    </div>
  </div>
</body>
<script src="<?php echo base_url()?>assets/template/css/login2/js/main.js"></script>
</html>        
