<div class="container">

      <div class="row"><br>
      <?php if($message)
      echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Error:</strong>'.$message.'
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
      ?>
            <?php echo form_open("auth/create_user");?>
                  <div class="col-md-6 ">
                        <div class="x_panel">
                              
                              <div class="x_content">
                                    <div class="form-group">
                                          <label class="col-md-12"><?php echo lang('create_user_fname_label', 'first_name');?></label>
                                          <div class="col-sm-12">
                                                <p><input type="text" name="first_name" value="" id="first_name" class="form-control" required>
                                                </p>
                                          </div>
                                          <label class="col-md-12"><?php echo lang('create_user_lname_label', 'last_name');?> <br /></label>
                                          <div class="col-sm-12">
                                                <input type="text" name="last_name" value="" id="last_name" class="form-control" required="">
                                          </div>

                                          <div class="col-sm-12"><br />
                                                <?php echo lang('create_user_email_label', 'email');?> 
                                                <input type="email" name="email" value="" id="email" class="form-control" required="">
                                          </div>

                                          <div class="col-sm-12"><br />
                                                <?php echo lang('create_user_phone_label', 'phone');?> 
                                                <?php echo form_input($phone);?>
                                          </div>
                                    </div>
                              </div>
                        </div>
                  </div>
                  <div class="col-md-6 ">
                        <div class="x_panel">
                              <div class="x_content">
                                    <div class="form-group">
                                          <div class="col-md-12"><br>
                                                <label>Nombre de Usuario</label>
                                                <input type="text" name="identity" value="" placeholder="username" id="identity" class="form-control col-md-12 text-normal" autocomplete="off" required>

                                                
                                          </div>
                                    </div>                              
                                    
                                    <div class="col-sm-6"><br />
                                          <?php echo lang('create_user_password_label', 'password');?> 
                                          <input type="password" name="password" id="password" minlength="8" class="form-control" requiered>
                                    </div>
                                    <div class="col-sm-6"><br />
                                          <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
                                          <input type="password" name="password_confirm" value="" id="password_confirm" class="form-control" minlength="8" required>
                                    </div>

                                    <div class="col-md-12"><br>
                                                <div class="x_title">GRUPO DE USUARIOS
                                                </div>
                                                <?php foreach ($groups as $group): ?>
                                                      <div class="checkbox" style="float: left;margin-right: 15px;border-right: 1px solid;padding-right: 15px;height:22px;">
                                                            <label class="">
                                                                  <div class="icheckbox_flat-green" style="position: relative;">
                                                                  <input name="groups[]" type="checkbox" class="flat" value="<?php echo $group->id ?>" style="position: absolute; opacity: 0;float: left;">
                                                                  <ins class="iCheck-helper icheckbox_flat-green checked" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                                                  </ins>
                                                                  </div> <?php echo $group->name ?>
                                                            </label>
                                                      </div>
                                                <?php endforeach; ?>
                                          </div>
                              </div>
                        </div>
                  </div>

            <div class="col-md-12">
                  <input type="submit" id="btn-guardar" name="submit" value="Crear Usuario" class="btn btn-sm btn-success">
                  <a type="button" class="btn btn-sm btn-danger" href="javascript:history.back(-1)">Cancelar</a>
                  </p>                                               
            </div>
            <?php echo form_close();?>
            
      </div>
</div>