<!DOCTYPE html>
<html lang="en">
      <head>

      <title>Login INDICIO</title>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="<?php echo base_url()?>assets/template/css/login2/images/favicon.png" />
      <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/login2/css/main.css' type='text/css' media='all' />
      <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/login2/css/util.css' type='text/css' media='all' />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/template/css/login2/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/template/css/login2/css/util.css">
      <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
      </head>

<body>

      <div class="limiter">
            <div class="container-login100">
            <div class="wrap-login100 p-t-85 p-b-20">
                  <span class="login100-form-avatar">
                        <img src="<?php echo base_url()?>assets/template/css/login2/images/logo.png" alt="AVATAR">
                  </span>
                        <h1><?php echo lang('change_password_heading');?></h1>
                        
						<div id="infoMessage"><?php echo $message;?></div>

						<?php echo form_open('auth/reset_password/' . $code);?>
							<div class="wrap-input100 validate-input m-t-85 m-b-35 rest-text" data-validate = "Enter old password">
							<p>
								<label  for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
								<?php echo form_input($new_password);?>
								<!--<span class="focus-input100" data-placeholder="Nueva Contraseña (mínimo 8 caracteres):"></span>-->
							</p>
							</div>
							<div class="wrap-input100 validate-input m-t-85 m-b-35 rest-text" data-validate = "Enter nueva contraseña">
							<p>
								<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
								<?php echo form_input($new_password_confirm);?>
								<!--<span class="focus-input100" data-placeholder="Confirmar Nueva Contraseña:"></span>-->
							</p>
							</div>
							<?php echo form_input($user_id);?>
							<?php echo form_hidden($csrf); ?>

							<p><?php echo form_submit('submit', lang('reset_password_submit_btn'),'class="login100-form-btn"');?></p>

						<?php echo form_close();?>

                  </div>
            </div>
      </div>

<body>
</html>

