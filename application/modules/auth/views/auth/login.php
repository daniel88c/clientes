<!DOCTYPE html>
<html lang="en">
<head>

  <title>Login INDICIO</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="<?php echo base_url()?>assets/template/css/login2/images/favicon.png" />
  <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/login2/css/main.css' type='text/css' media='all' />
  <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/login2/css/util.css' type='text/css' media='all' />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/template/css/login2/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/template/css/login2/css/util.css">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>

<body>
  
  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100 p-t-85 p-b-20">
      <span class="login100-form-avatar">
            <img src="<?php echo base_url()?>assets/template/css/login2/images/logo.png" alt="AVATAR">
      </span>

      <h1><?php echo lang('login_heading');?></h1>
      

      <div id="infoMessage"><?php echo $message;?></div>

      <?php echo form_open("auth/login");?>
      <div class="wrap-input100 validate-input m-t-85 m-b-35" data-validate = "Email/Username">
        <p>

          <?php echo form_input($identity);?>
          <span class="focus-input100" data-placeholder="Email/Username"></span>
          
        </p>
      </div>
      <div class="wrap-input100 validate-input m-b-50" data-validate="Enter password">
        <p>
          <?php echo form_input($password);?>
          <span class="focus-input100" data-placeholder="Contraseña"></span>
        </p>
      </div>
      <div>
        <p>
          <?php echo lang('login_remember_label', 'remember');?>
          <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
        </p>
      </div>
      <div class="boton-login">
        <p><?php echo form_submit('submit', lang('login_submit_btn'), 'class="login100-form-btn"');?></p>
      </div>
      
      <?php echo form_close();?>

      <p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>

      </div>
    </div>
  </div>
 
  <div id="dropDownSelect1"></div>
  <script src="<?php echo base_url()?>assets/template/css/login2/js/main.js"></script>

</body>
</html>      