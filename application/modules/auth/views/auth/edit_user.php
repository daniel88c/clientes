<div class="container">

      <div class="row"><br>
      <?php if($message)
      echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Error:</strong>'.$message.'
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
      ?>
      <?php echo form_open(uri_string());?>
        <div class="col-md-6">
          <div class="x_panel">
            <div class="x_content">
              <div class="form-group">
              <p>
                    <?php echo lang('edit_user_fname_label', 'first_name');?> <br />
                    <?php echo form_input($first_name);?>
              </p>

              <p>
                    <?php echo lang('edit_user_lname_label', 'last_name');?> <br />
                    <?php echo form_input($last_name);?>
              </p>
              <p>
                    <?php echo lang('create_user_email_label', 'email');?> 
                    <?php echo form_input($email);?>
                    
              </p>
              <p>
                    <?php echo lang('edit_user_phone_label', 'phone');?> <br />
                    <?php echo form_input($phone);?>
              </p>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-sm-6">
          <div class="x_panel">
            <div class="x_content">
              <div class="form-group">
              <p>
                <label>Nombre de Usuario</label>
                <input type="text" value="<?php echo $user->username?>" placeholder="username" id="identity" class="form-control col-md-12 text-normal" disabled>
              </p><br><br>
              <p>
                    <?php echo lang('edit_user_password_label', 'password');?> <br />
                    <?php echo form_input($password);?>
              </p>

              <p>
                    <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
                    <?php echo form_input($password_confirm);?>
              </p>
          
            <?php if ($this->ion_auth->is_admin()): ?>

                <h2><?php echo lang('edit_user_groups_heading');?></h2>
                <?php $i=0;
                 foreach ($currentGroups as $g){
                  $grupos_act[$i] = $g['id'];
                  $i++;
                }?>
                <?php foreach ($groups as $group):?>
                    <label class="checkbox">
                    <div class="icheckbox_flat-green checked" style="position: relative;">
                    <input type="checkbox" name="groups[]" class="flat" value="<?php echo $group['id'];?>" <?php echo (in_array($group['id'], $grupos_act)) ? 'checked="checked"' : null; ?> style="position: absolute; opacity: 0;">
                        <ins class="iCheck-helper icheckbox_flat-green checked" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                        </ins>
                    </div>                  
                    <?php echo htmlspecialchars($group['name'].' | ',ENT_QUOTES,'UTF-8');?>
                    </label>
                <?php endforeach?>

            <?php endif ?>

            <?php echo form_hidden('id', $user->id);?>
            <?php echo form_hidden($csrf); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
         <p><?php echo form_submit('submit', lang('edit_user_submit_btn'), 'class="btn btn-sm btn-success"');?>
            <a type="button" class="btn btn-sm btn-danger" href="<?php echo base_url()?>auth" title="Cancelar">Cancelar</a>
            </p> 
      </div>
                      
      <?php echo form_close();?>
  </div>
</div>