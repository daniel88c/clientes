<!--<h1><?php echo lang('create_group_heading');?></h1>-->
<p><?php echo lang('create_group_subheading', 'sub-tit', 'class="sub-tit"');?></p>

<div id="infoMessage">
      <?php if($message)
      echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Error:</strong>'.$message.'
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
      ?>
</div>

<div class="container">
      <?php echo form_open("auth/create_group");?>
            <div class="col-md-6">
                  <div class="x_panel">
                  <div class="x_content">
                        <div class="form-group">
                              <label class="col-md-12">
                                    <?php echo lang('create_group_name_label', 'group_name');?> <br />
                              </label>
                              <div class="col-sm-12">
                                    <?php echo form_input($group_name);?>
                              </div>                                    
                              
                              <br>
                              <label class="col-md-12"><br>
                                    <?php echo lang('create_group_desc_label', 'description');?> <br />
                              </label>
                              <div class="col-sm-12">
                                    <?php echo form_input($description);?>
                              </div>
                              <div class="col-sm-12"><br>
                              <?php echo form_submit('submit', lang('create_group_submit_btn'), 'class="btn btn-sm btn-success"');?>
                              <a type="button" class="btn btn-sm btn-danger" href="javascript:history.back(-1);" title="Cancelar">Cancelar</a>
                              </div>
                        </div>
                  </div>                              
                  </div>
            </div>
            <div class="col-sm-6">
                  <div class="x_panel">
                        <div class="x_content">
                              <label>Permisos</label><br>
                              
                              <?php foreach ($menus as $m): ?>
                              <div class="checkbox" style="float: left;margin-right: 15px;border-right: 1px solid;padding-right: 15px;height:22px;">
                                    <label class="">
                                          <div class="icheckbox_flat-green" style="position: relative;">
                                          <input name="menus[]" type="checkbox" class="flat" <?php if($m->id == 1) echo "checked"?> value="<?php echo $m->id ?>" style="position: absolute; opacity: 0;float: left;">
                                          <ins class="iCheck-helper icheckbox_flat-green checked" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                          </ins>
                                          </div> <?php echo $m->nombre ?>
                                    </label>
                              </div>
                        <?php endforeach; ?>
                        </div>
                  </div>
            </div>
            <?php echo form_close();?>

      </div>
</div>