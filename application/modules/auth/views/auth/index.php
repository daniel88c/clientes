<div class="col-md-12">
<?php if($message)
	      echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
	        <strong>'.$message.'</strong>
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>';
	      ?>
</div>
<div class="row">

	<div class="x_panel">
		<?php echo anchor('auth/create_user', '<i class="fa fa-plus"></i> '.lang('index_create_user_link'), 'class="btn btn-sm btn-info"')?> | <?php echo anchor('auth/create_group', '<i class="fa fa-plus"></i> '.lang('index_create_group_link'), 'class="btn btn-sm btn-info"')?>
		<table cellpadding=0 cellspacing=10 id="tablas00" class="datatable table table-striped jambo_table bulk_action" >
			<thead>
			<tr>
				<th>Nombre de Usuario</th>
				<th><?php echo lang('index_fname_th');?></th>
				<th><?php echo lang('index_lname_th');?></th>
				<th><?php echo lang('index_email_th');?></th>
				<th style="width: 100px"><?php echo lang('index_groups_th');?></th>
				<th style="width: 100px"><?php echo lang('index_status_th');?></th>
				<th style="width: 100px"><?php echo lang('index_action_th');?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($users as $user):?>
				<tr>
		            <td><?php echo htmlspecialchars($user->username,ENT_QUOTES,'UTF-8');?></td>
		            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
		            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
		            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
					<td>
						<?php foreach ($user->groups as $group):?>
							<?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8') ?>
							<br />
		                <?php endforeach?>
					</td>
					<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link'),'class=" activa badge badge-success"') : anchor("auth/activate/". $user->id, lang('index_inactive_link'),'class="badge badge-warning"');?></td>
					<td><?php echo anchor("auth/edit_user/".$user->id, 'Editar','class="badge badge-primary"') ;?>
						<button class='btn btn-sm badge badge-danger' onclick="baja('users',<?php echo $user->id; ?>)">Eliminar</button>
					</td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
</div>