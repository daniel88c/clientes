<?php
class General_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){

    $this->db->select($fields);
    $this->db->from($table);
    $this->db->limit($perpage,$start);
    if($where)
      $this->db->where($where);

    $query = $this->db->get();

    $result =  !$one  ? $query->result($array) : $query->row() ;
    return $result;
  }

  function getAll($table,$fields,$where='',$order= null ,$array = null){

    $this->db->select($fields);

    if($where){
      $this->db->where($where);
    }

    if($order){
      $this->db->order_by($order);
    }

    $query = $this->db->get($table);
    if (is_null($array))
      $result = $query->result();
    else
      $result = $query->result($array);

    return $result;
  }


  function baja($table_name, $id){
      $data = array(
          'baja' => 1,
          'tmstmp_upd' => date('Y-m-d H:i:s'),
          'user_upd' => $this->session->userdata('user_id')
          );
      
      $this->db->where('id',$id)
      ->update($table_name, $data);
      return $data;    
  }

  function getGroupByName($name)  {
    $query = $this->db
    ->select('*')
    ->where('name',$name)
    ->get('groups');
    return $query->row();
  }

  function getLocByProvincia($provincia_id){
    $query = $this->db->select("localidad.id, localidad.nombre, localidad.departamento")
    ->join("provincia","localidad.provincia_id = provincia.id", "inner")
    ->where("provincia.id", $provincia_id)
    ->order_by("nombre")
    ->get("localidad");
    $data = $query->result();
    return $data;
  }

  public function alta_localidad($nueva_localidad, $provincia_id)
  {
     $data = array(
        'provincia_id' => $provincia_id,
        'nombre' => $nueva_localidad
        //'user_id' => $this->session->userdata('user_id')
        );

    $this->db->insert('localidad', $data, 'strlen');
    $localidad_id = $this->db->insert_id();

    return $localidad_id;
  }

  public function alta_domicilio($dom_calle,$dom_numero,$provincia_id,$localidad_id,$codigo_postal)
  {
    $data = array(
        'provincia_id' => $provincia_id,
        'localidad_id' => $localidad_id,
        'otra_localidad' => null,        
        'domicilio_calle' => trim($dom_calle),
        'domicilio_numero' => trim($dom_numero),
        'codigo_postal' => trim($codigo_postal),
        'user_id' => $this->session->userdata('user_id')
        );

    $this->db->insert('domicilio', $data, 'strlen');
    $domicilio_id = $this->db->insert_id();

    return $domicilio_id;
    
  }

  public function update_domicilio($domicilio_id
               ,$provincia_id
               ,$localidad_id
               ,$otra_localidad
               ,$dom_calle
               ,$dom_numero
               ,$codigo_postal
               ){
  
      $data = array(
          'provincia_id' => $provincia_id,
          'localidad_id' => $localidad_id,
          'otra_localidad' => $otra_localidad,                  
          'domicilio_calle' => trim($dom_calle),
          'domicilio_numero' => trim($dom_numero),
          'codigo_postal' => trim($codigo_postal),        
          'user_upd' => $this->session->userdata('user_id')
          );
      
      $this->db->where('id',$domicilio_id)
      ->update('domicilio', $data);
      return $data;
  }

  function alta_variable($table, $name, $padre_id = null, $name_padre = null){
    if ($name_padre)
      $data = array(
        'nombre' => $name,
        $name_padre => $padre_id,
        'user_id' => $this->session->userdata('user_id')      
      );
    else
      $data = array(
        'nombre' => $name,
        'user_id' => $this->session->userdata('user_id')      
      );

    $this->db->insert($table, $data);
    $nuevo_id = $this->db->insert_id();

    return $nuevo_id;

  }

}