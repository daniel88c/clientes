<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Code here is run before ALL controllers
class MY_Controller extends CI_Controller
{
	var $user;

	function __construct()
	{
		parent::__construct();
		$session = $this->session->userdata('identity');

		if (!$this->ion_auth->logged_in() or !$session)
		{
			redirect("auth");
		}
		$CI =& get_instance();

		$CI->data['user'] = $this->ion_auth->user();

		//$this->output->enable_profiler();
	}
	
	protected function after(){}
	
	/*public function _remap($method, $args){
		// Call before action
		$this->before();
		$array = array($this, $method);
		if(sizeof($array)>0)
		{
			call_user_func_array($array, $args);
		}
		else
		{
	   		$this->output->set_status_header('404');
			// Salida pagina error 404
		}
   }*/
        
    /**
     * These shouldn't be accessible from outside the controller
    **/
    protected function before() {}
}


class MY_Controller_Public extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

	}
	
	protected function after(){}
	
	public function _remap($method, $args){
		// Call before action
		$this->before();
		$array = array($this, $method);
		if(sizeof($array)>0)
		{
			call_user_func_array($array, $args);
		}
		else
		{
	   		$this->output->set_status_header('404');
			// Salida pagina error 404
		}
   }
        
    /**
     * These shouldn't be accessible from outside the controller
    **/
    protected function before() {}
}