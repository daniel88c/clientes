<?php
  //$this->load->model('general_model', 'general_md');
  $this->load->model('auth/ion_auth_model','ion_auth'); 
  $this->load->model('permisos/permisos_md');


  $grupos = $this->ion_auth->get_users_groups()->result();
    $i = 0;
    $config = 0;
    foreach ($grupos as $gr) {
      $grupos_ids[$i] = $gr->id;
      if ($gr->id == 1) {
        $config = 1;
      }
      $i++;
    }

  $menus = $this->permisos_md->getPermisosByGroupId($grupos_ids);

?>

<div class="col-md-3 left_col menu_fixed">
	<div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo base_url();?>dashboard" class="site_title"><img src="<?php echo base_url();?>/assets/gentella/custom/images/logo.png" width="100%" alt=""> </a>
        </div>

        <div class="clearfix"></div>

            <!-- menu profile quick info 
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>John Doe</h2>
              </div>
            </div>
            /menu profile quick info -->

            <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        	<div class="menu_section">
            	
	            <ul class="nav side-menu">

                <?php
                  foreach ($menus as $menu):
                    if ($menu->link != '#') {
                      echo '<li><a href="'.base_url().$menu->link.'"><i class="'.$menu->icono.'"></i> '.$menu->nombre.'</a>
                      </li>';
                    }
                    else{
                      echo '<li><a><i class="'.$menu->icono.'"></i> '.$menu->nombre.'<span class="fa fa-chevron-down"></span></a>';
                      echo '<ul class="nav child_menu">';

                      $submenus = $this->permisos_md->getSubmenus($menu->id);

                      foreach ($submenus as $sub) {
                        echo '<li><a href="'.base_url().$sub->link.'">'.$sub->nombre.'</a>
                      </li>';
                      }
                      echo '</ul></li>';
                      
                    }
                    
                  endforeach;
                ?>
        
                  <!-- <li><a><i class="fa fa-minus"></i> TEST<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url()?>servicios/create">Servicios</a></li>
                      <li><a href="<?php echo base_url()?>certificaciones">Certificaciones</a></li>
                      <li><a href="<?php echo base_url()?>propuestas">Propuestas</a></li>
                    </ul>
                  </li> -->
                <!-- Develop -->
                <!-- develop -->
                
	            </ul>
        	</div>
		</div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
          <a data-toggle="tooltip" data-placement="top" title="Configuraciones" href="<?php if($config) echo base_url().'permisos'; else echo '#'?>">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
          </a>
          <a data-toggle="tooltip" id="btn-fscreen" onclick="Fullscreen()" class="active" data-placement="top" title="FullScreen">
            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
          </a>
          <a data-toggle="tooltip" data-placement="top" title="Bloquear">
            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
          </a>
          <a data-toggle="tooltip" data-placement="top" title="Cerrar session" href="<?php echo base_url()?>auth/logout">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
          </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>