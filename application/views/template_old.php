<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es-AR">

<!--HEAD-->
<?php include 'head.php'?>
<!--/HEAD-->

<body class="home-page home page body_style_fullwide body_filled article_style_stretch top_panel_style_dark top_panel_opacity_solid top_panel_over sidebar_hide responsive_menu vc_responsive no-js">
<div class="body_wrap">
   
    <div class="page_wrap">
        <div id="page">
            <!-- HEADER-->
            <?php include 'header.php'?>
            <!-- /HEADER-->

            <div id="mainContent" class="content">
                <!-- CONTENIDO-->
                <?php echo $contenido_main; ?>
                <!-- -->
            </div>
                <!-- MENU-->
                <?php include 'menu.php'?>
                <!-- /MENU-->
            <div class="copyright_wrap">
            <div class="content_wrap">
                <p>© 2020 INDICIO. Todos los derechos reservados.
                    <a href="#">Términos de usos</a> y
                    <a href="#">Políticas de privacidad</a>
                </p>
            </div>
        </div>
        <a href="#" class="scroll_to_top icon-angle-up-1" title="Scroll to top"></a>
        </div>
    </div>
    
</div>

<div class="custom_html_section"></div>

<!-- SCRIPT templete-->
    <?php include 'script-templete.php'?>
<!-- /SCRIPT templete-->

<?php echo $jscript ?>

</body>
</html>