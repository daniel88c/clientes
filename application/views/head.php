<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/x-icon" href="<?php echo base_url()?>assets/template/css/login2/images/favicon.png" />
    <title>Sistema Los Tres Cerritos</title>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700|Love+Ya+Like+A+Sister|Montserrat:100,200,300,400,500,600,700,800,900|Open+Sans:300,300i,400,400i,600,700,700i,800|Raleway:100,200,300,400,500,600,700,800,900|Roboto:100,100i,300,300i,400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext' type='text/css' media='all' />
    
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/public.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/fontello/css/fontello.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/shortcodes.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/core.animation.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/js/vendor/bp/bbpress-style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/skin.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/custom-style.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/responsive.css' type='text/css' media='all' /> 
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/custom.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/js/vendor/comp/comp.min.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/core.messages.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/flipclock.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?php echo base_url()?>assets/template/js/vendor/swiper/idangerous.swiper.min.css' type='text/css' media='all' />    
	<link rel='stylesheet' href='<?php echo base_url()?>assets/template/css/notie.css' type='text/css' media='all' />
    <link href="<?php echo base_url()?>assets/fontawesome/css/all.css" rel="stylesheet">
    <link rel='stylesheet' href='<?php echo base_url()?>assets/fontawesome/css/fontawesome.min.css' type='text/css' media='all' />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/datatable/datatables.css">


    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/mmenu-js/mmenu.css" />

    <style type="text/css">
        :root {
            --mm-sidebar-collapsed-size: 72px;
            --mm-sidebar-expanded-size: 300px;
        }
        svg:not(:root).svg-inline--fa,svg:not(:root).svg-inline--fas{
            padding: 0px 18px;
        }
        svg:not(:root).svg-inline--fa.fa-search{
            margin-left: 15px;
        }
    </style>

    <script defer src="<?php echo base_url()?>assets/fontawesome/js/all.js"></script>

</head>