<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="icon" type="image/x-icon" href="<?php echo base_url()?>assets/template/css/login2/images/favicon.png" />
    <title>Los 3 Cerritos | </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()?>assets/gentella/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url()?>assets/gentella/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url()?>assets/gentella/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url()?>assets/gentella/iCheck/skins/flat/green.css" rel="stylesheet">
	<!-- Switchery -->
    <link href="<?php echo base_url()?>assets/gentella/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url()?>assets/gentella/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url()?>assets/gentella/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url()?>assets/gentella/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- datatables -->
    <link href="<?php echo base_url()?>assets/gentella/datatables/datatables.css">
    <!-- select2 -->
    <link href="<?php echo base_url()?>assets/gentella/select2/dist/css/select2.min.css" rel="stylesheet" />
    <!-- jquery UI -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/gentella/jquery.ui/jquery-ui.css">
    <!-- lightbox 2 -->
    <link href="<?php echo base_url()?>assets/lightbox2/css/lightbox.css" rel="stylesheet" />


    <!-- Custom Theme Style -->
    <link href="<?php echo base_url()?>assets/gentella/custom/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
       <div class="main_container">

        <!-- MENU-->
          <?php include 'menu.php'?>
        <!-- /MENU-->

        <!-- HEADER-->
            <?php include 'header.php'?>
        <!-- /HEADER-->


        
        <!-- page content -->
        <div id="mainContent" class="content right_col" role="main">
                <!-- CONTENIDO-->
                <?php echo $contenido_main; ?>
                <!-- -->
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Los Tres Cerritos - Todos los derechos  | <a target="_blank" href="http://bitabit.com.ar">Develop by BIT A BIT</a> | Powered by Vision Global
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
     
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url()?>assets/gentella/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url()?>assets/gentella/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url()?>assets/gentella/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url()?>assets/gentella/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url()?>assets/gentella/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url()?>assets/gentella/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url()?>assets/gentella/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url()?>assets/gentella/iCheck/icheck.min.js"></script>
    <!-- Switchery -->
    <script src="<?php echo base_url()?>assets/gentella/switchery/dist/switchery.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url()?>assets/gentella/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url()?>assets/gentella/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url()?>assets/gentella/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url()?>assets/gentella/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url()?>assets/gentella/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url()?>assets/gentella/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/bootstrap-daterangepicker/daterangepicker.js"></script>

    < <!-- Datatables -->
    <script src="<?php echo base_url()?>assets/gentella/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url()?>assets/gentella/pdfmake/build/vfs_fonts.js"></script>

    <!-- Select 2 -->
    <script src="<?php echo base_url()?>assets/gentella/select2/dist/js/select2.min.js"></script>
    <!-- jquery UI -->
    <script src="<?php echo base_url()?>assets/gentella/jquery.ui/jquery-ui.js"></script>
    <!--  sweetalert2 -->
        <script src="<?php echo base_url()?>assets/sweetalert2/sweetalert2.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url()?>assets/gentella/custom/js/custom.min.js"></script>
    <!-- File UPLOAD progress -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/files/upload.js"></script>
    <!-- lightbox 2 -->
    <script src="<?php echo base_url()?>assets/lightbox2/js/lightbox.js"></script>

    
    
    <?php include 'script-templete.php'?>
    <?php echo $jscript ?>


	
  </body>
</html>
